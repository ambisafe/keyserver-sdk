package co.ambisafe.keyserver.schema;

import co.ambisafe.keyserver.dto.internal.AccountWrapper;
import co.ambisafe.keyserver.dto.internal.ContainerWrapper;
import co.ambisafe.keyserver.dto.internal.TransactionResponse;
import co.ambisafe.keyserver.dto.request.AccountRequest;
import co.ambisafe.keyserver.dto.request.SignTransactionRequest;
import co.ambisafe.keyserver.dto.request.TransactionRequest;
import co.ambisafe.keyserver.exception.BuildTransactionException;
import co.ambisafe.keyserver.exception.NotSignedException;
import co.ambisafe.keyserver.exception.SuperNodeException;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Properties;

public interface SecuritySchema {
    public String getSchemaName();

    public void setEnvironment(Environment environment);

    public void beforeCreateContainers(AccountRequest request, AccountWrapper data);

    public void createContainers(AccountRequest request, AccountWrapper data);

    public void afterCreateContainers(AccountRequest request, AccountWrapper data);

    public void beforeUpdateContainers(AccountRequest request, AccountWrapper data);

    public void updateContainers(AccountRequest request, AccountWrapper data);

    public void afterUpdateContainers(AccountRequest request, AccountWrapper data);

    public void beforeBuildTransaction(TransactionRequest request, AccountWrapper data) throws BuildTransactionException;

    public TransactionResponse buildTransaction(TransactionRequest request, AccountWrapper data) throws BuildTransactionException, SuperNodeException;

    public void afterBuildTransaction(TransactionRequest request, AccountWrapper accountData, TransactionResponse transaction);

    public void beforeBuildRecoveryTransaction(TransactionRequest request, AccountWrapper from, AccountWrapper to);

    public TransactionResponse buildRecoveryTransaction(TransactionRequest request, AccountWrapper from, AccountWrapper to);

    public void afterBuildRecoveryTransaction(TransactionResponse response, TransactionRequest request, AccountWrapper from, AccountWrapper to);

    public void beforeSignTransaction(SignTransactionRequest request, AccountWrapper data);

    public TransactionResponse signTransaction(SignTransactionRequest request, AccountWrapper data);

    public void afterSignTransaction(TransactionResponse response, SignTransactionRequest request, AccountWrapper data);

    public void beforeGetAccountContainers(AccountRequest request, AccountWrapper data);

    public List<ContainerWrapper> getAccountContainers(AccountRequest request, AccountWrapper data);

    public void afterGetAccountContainers(List<ContainerWrapper> containers, AccountRequest request, AccountWrapper data);
}