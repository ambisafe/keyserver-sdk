package co.ambisafe.keyserver.schema.impl;

import co.ambisafe.keyserver.dto.internal.AccountWrapper;
import co.ambisafe.keyserver.dto.internal.ContainerWrapper;
import co.ambisafe.keyserver.dto.internal.TransactionResponse;
import co.ambisafe.keyserver.dto.request.AccountRequest;
import co.ambisafe.keyserver.dto.request.SignTransactionRequest;
import co.ambisafe.keyserver.dto.request.TransactionRequest;
import co.ambisafe.keyserver.exception.BuildTransactionException;
import co.ambisafe.keyserver.exception.NotSignedException;
import co.ambisafe.keyserver.schema.SecuritySchema;
import co.ambisafe.keyserver.service.ContainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

public abstract class AbstractSecuritySchema implements SecuritySchema {
    protected ContainerService containerService;

    @Autowired
    public void setContainerService(ContainerService containerService) {
        this.containerService = containerService;
    }

    @Override
    public void beforeCreateContainers(AccountRequest request, AccountWrapper data) {

    }

    @Override
    public void afterCreateContainers(AccountRequest request, AccountWrapper data) {

    }

    @Override
    public void beforeUpdateContainers(AccountRequest request, AccountWrapper data) {

    }

    @Override
    public void afterUpdateContainers(AccountRequest request, AccountWrapper data) {

    }

    @Override
    public void beforeBuildTransaction(TransactionRequest request, AccountWrapper data) throws BuildTransactionException {

    }

    @Override
    public void afterBuildTransaction(TransactionRequest request, AccountWrapper accountData, TransactionResponse transaction) {

    }

    @Override
    public void beforeBuildRecoveryTransaction(TransactionRequest request, AccountWrapper from, AccountWrapper to) {

    }

    @Override
    public void afterBuildRecoveryTransaction(TransactionResponse response, TransactionRequest request, AccountWrapper from, AccountWrapper to) {

    }

    @Override
    public void beforeSignTransaction(SignTransactionRequest request, AccountWrapper data) {

    }

    @Override
    public void afterSignTransaction(TransactionResponse response, SignTransactionRequest request, AccountWrapper data) {

    }

    @Override
    public void beforeGetAccountContainers(AccountRequest request, AccountWrapper data) {

    }

    @Override
    public void afterGetAccountContainers(List<ContainerWrapper> containerWrappers, AccountRequest request, AccountWrapper data) {

    }
}
