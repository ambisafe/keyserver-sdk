package co.ambisafe.keyserver.dto.request;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class SignTransactionRequest extends Request {
    public String hex;
    public List<String> sighashes;

    public SignTransactionRequest(JsonNode json) {
        super(json);
    }
}
