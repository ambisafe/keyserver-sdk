package co.ambisafe.keyserver.dto.request;

import com.fasterxml.jackson.databind.JsonNode;

public class TransactionRequest extends Request {
    public String currency;
    public String destination;
    public String amount;
    public String hex;

    public TransactionRequest() {
        super(null);
    }

    public TransactionRequest(JsonNode jsonNode) {
        super(jsonNode);
    }
}
