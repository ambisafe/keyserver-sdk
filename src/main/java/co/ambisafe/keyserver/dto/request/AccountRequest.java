package co.ambisafe.keyserver.dto.request;

import com.fasterxml.jackson.databind.JsonNode;

public class AccountRequest extends Request {
    private String externalId;
    private String accountId;

    public AccountRequest(JsonNode json) {
        super(json);
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
