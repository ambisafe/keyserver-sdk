package co.ambisafe.keyserver.dto.request;

import com.fasterxml.jackson.databind.JsonNode;

public class Request {
    private JsonNode json;

    public Request(JsonNode json) {
        this.json = json;
    }

    public JsonNode getJson() {
        return json;
    }

    public void setJson(JsonNode json) {
        this.json = json;
    }
}