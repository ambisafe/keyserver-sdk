package co.ambisafe.keyserver.dto.internal;

public class KeyPair {

    private byte[] privKey;
    private byte[] pubKey;

    public KeyPair() {
    }

    public KeyPair(byte[] privKey, byte[] pubKey) {
        this.privKey = privKey;
        this.pubKey = pubKey;
    }

    public byte[] getPrivKey() {
        return privKey;
    }

    public void setPrivKey(byte[] privKey) {
        this.privKey = privKey;
    }

    public byte[] getPubKey() {
        return pubKey;
    }

    public void setPubKey(byte[] pubKey) {
        this.pubKey = pubKey;
    }
}
