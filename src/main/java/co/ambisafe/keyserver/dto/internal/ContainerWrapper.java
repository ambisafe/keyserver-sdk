package co.ambisafe.keyserver.dto.internal;

public class ContainerWrapper {
    public long id;

    public String roleName;
    public int role;

    public byte[] publicKey;

    public byte[] data;

    public ContainerWrapperData unparsedData;

    public ContainerWrapper(int role, String roleName, byte[] publicKey, byte[] data) {
        this.role = role;
        this.roleName = roleName;
        this.publicKey = publicKey;
        this.data = data;
    }

    public byte[] getPublicKey() {
        return publicKey;
    }
}
