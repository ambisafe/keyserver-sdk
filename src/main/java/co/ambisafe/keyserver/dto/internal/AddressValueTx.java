package co.ambisafe.keyserver.dto.internal;

public class AddressValueTx {
    public String sender;
    public String destination;
    public String value;
    public String txHash;

    public AddressValueTx(String sender, String destination, String value, String txHash) {
        this.sender = sender;
        this.destination = destination;
        this.value = value;
        this.txHash = txHash;
    }
}
