package co.ambisafe.keyserver.dto.internal;

import java.io.Serializable;
import java.math.BigDecimal;

public class CryptoCurrencyBalance implements Serializable {
    private static final long serialVersionUID = 1L;

	private String currencySymbol;
    private String address;
    private String balanceInSatoshis;
    private String balance;

    public CryptoCurrencyBalance(String currencySymbol, String address, BigDecimal balance) {
        this.currencySymbol = currencySymbol;
        this.address = address;
        this.balanceInSatoshis = balance.toString();
        this.balance = balance.toPlainString();
    }

    public CryptoCurrencyBalance(String currencySymbol, String address, String balance, String balanceInSatoshis) {
        this.currencySymbol = currencySymbol;
        this.address = address;
        this.balanceInSatoshis = balanceInSatoshis;
        this.balance = balance;
    }

    public CryptoCurrencyBalance(String currencySymbol, String address, String balance) {
        this.currencySymbol = currencySymbol;
        this.address = address;
        this.balance = balance;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public String getAddress() {
        return address;
    }

    public String getBalanceInSatoshis() {
        return balanceInSatoshis;
    }

    public String getBalance() {
        return balance;
    }
}