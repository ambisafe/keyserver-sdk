package co.ambisafe.keyserver.dto.internal;

import co.ambisafe.keyserver.asset.CryptoAsset;
import co.ambisafe.keyserver.schema.SecuritySchema;

import java.util.*;

public class AccountWrapper {
    public CryptoAsset asset;
    public SecuritySchema securitySchema;
    public List<ContainerWrapper> containers = new ArrayList<>();

    public long id;

    public String externalId;

    public String currencyFamilySymbol;

    public String securitySchemaName;

    public String address;

    public Date createdAt;

    public Date updatedAt;

    public List<ContainerWrapper> getContainersByRole(int role) {
        List<ContainerWrapper> containers = new ArrayList<ContainerWrapper>();

        for(ContainerWrapper containerWrapper : this.containers) {
            if(containerWrapper.role == role) {
                containers.add(containerWrapper);
            }
        }

        return containers;
    }

    public Optional<ContainerWrapper> getFirstContainerByRole(int role) {
        for(ContainerWrapper containerWrapper : this.containers) {
            if(containerWrapper.role == role) {
                return Optional.of(containerWrapper);
            }
        }

        return Optional.empty();
    }

    public List<byte[]> getAllPubKeys() {
        SortedSet<ContainerWrapper> sortedSet = new TreeSet<>(new Comparator<ContainerWrapper>() {
            @Override
            public int compare(ContainerWrapper o1, ContainerWrapper o2) {
                if(o1.role == o2.role) {
                    return 0;
                }

                if(o1.role > o2.role) {
                    return 1;
                }

                return -1;
            }
        });

        for(ContainerWrapper containerWrapper : containers) {
            sortedSet.add(containerWrapper);
        }

        List<byte[]> pubKeys = new ArrayList<byte[]>();

        for(ContainerWrapper containerWrapper : sortedSet) {
            pubKeys.add(containerWrapper.publicKey);
        }

        return pubKeys;
    }
}