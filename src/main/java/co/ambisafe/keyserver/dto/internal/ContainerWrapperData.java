package co.ambisafe.keyserver.dto.internal;

public class ContainerWrapperData {
    public byte[] iv;
    public String salt;
    public byte[] data;

    public ContainerWrapperData(byte[] iv, String salt, byte[] data) {
        this.iv = iv;
        this.salt = salt;
        this.data = data;
    }
}
