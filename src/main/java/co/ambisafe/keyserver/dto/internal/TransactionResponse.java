package co.ambisafe.keyserver.dto.internal;

import java.util.List;

public class TransactionResponse {
    public String hex;
    public String fee;
    public List<String> sighashes;

    public TransactionResponse(String hex, String fee, List<String> sighashes) {
        this.hex = hex;
        this.fee = fee;
        this.sighashes = sighashes;
    }

    public TransactionResponse() {
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BuildTransaction{");
        sb.append("hex='").append(hex).append('\'');
        sb.append(", fee='").append(fee).append('\'');
        sb.append(", sighashes=").append(sighashes);
        sb.append('}');
        return sb.toString();
    }
}