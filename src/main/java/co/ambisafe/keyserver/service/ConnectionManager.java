package co.ambisafe.keyserver.service;

import co.ambisafe.keyserver.supernode.SuperNode;
import co.ambisafe.keyserver.supernode.SuperNodeCollection;

import java.util.List;

public interface ConnectionManager {
    SuperNode getSuperNode(List<String> superNodes);
    SuperNode getSuperNode(List<String> superNodes, Class implementedClass);
}