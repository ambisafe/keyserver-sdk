package co.ambisafe.keyserver.service;

import co.ambisafe.keyserver.dto.internal.AccountWrapper;
import co.ambisafe.keyserver.dto.internal.ContainerWrapper;
import co.ambisafe.keyserver.dto.internal.ContainerWrapperData;
import co.ambisafe.keyserver.dto.request.AccountRequest;
import com.fasterxml.jackson.databind.JsonNode;

public interface ContainerService {
    ContainerWrapper getContainerFromRequest(AccountRequest request, String roleKey, int role);
    ContainerWrapper getContainerFromRequest(AccountRequest request, JsonNode containerNode, String roleName, int role);
    void createRandomKeysContainer(AccountWrapper data, String roleKey, int role, String secret);
    byte[] getPrivateKeyForContainer(ContainerWrapper container, String secret);
    void removeContainerByRole(AccountWrapper wrapper, int role);

    ContainerWrapperData parseContainerData(ContainerWrapper containerWrapper);

    AccountWrapper getRecoveryAccountWithContainers(String address);
}