package co.ambisafe.keyserver.asset;

import co.ambisafe.keyserver.dto.internal.AddressValueTx;
import co.ambisafe.keyserver.dto.internal.CryptoCurrencyBalance;
import co.ambisafe.keyserver.dto.internal.KeyPair;
import co.ambisafe.keyserver.dto.internal.TransactionResponse;
import co.ambisafe.keyserver.exception.SuperNodeException;
import co.ambisafe.keyserver.supernode.SuperNode;

import java.util.List;
import java.util.Set;

public interface CryptoAsset extends AssetFamily {
    TransactionResponse buildTransaction(int threshold, List<byte[]> keys, String destination, String amount);

    TransactionResponse buildEmptyAddressTransaction(int threshold, List<byte[]> keys, String destination);

    List<String> sign(byte[] privateKey, List<String> sighashes);

    // key is needed to determine where to insert the signature
    String insertSignatures(String rawTransactionHex, List<String> signatures, byte[] key);

    CryptoCurrencyBalance getBalance(String address);

    KeyPair getRandomKeyPair();

    String getSymbol();

    // Verifies if signatures are sighahses signed by provided key.
    boolean verifySignatures(List<String> sighashes, List<String> signatures, byte[] key);

    // Verifies SigScript of all inputs is valid and fully signed
    void verifySignatures(String rawTxHex);

    boolean transactionsNotifications();

    int getNewBlockCheckInterval();

    List<AddressValueTx> findAddressesInTransaction(Set<String> addresses, String rawTransaction);

    void checkAddresses(String rawTransaction, String sender, String destination);

    String getPostbackUrl();

    SuperNode getSuperNode();
}