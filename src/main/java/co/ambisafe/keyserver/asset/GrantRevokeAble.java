package co.ambisafe.keyserver.asset;

import co.ambisafe.keyserver.dto.internal.TransactionResponse;
import co.ambisafe.keyserver.exception.SuperNodeException;

import java.util.List;

public interface GrantRevokeAble {

    TransactionResponse grant(int threshold, List<byte[]> keys, String destination, String amount);

    TransactionResponse revoke(int threshold, List<byte[]> keys, String destination, String amount) throws SuperNodeException;
}
