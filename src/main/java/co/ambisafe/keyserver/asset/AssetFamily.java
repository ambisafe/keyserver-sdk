package co.ambisafe.keyserver.asset;

import java.util.List;

public interface AssetFamily {
    String getFamilySymbol();

    String getMultisigAddress(int threshold, List<byte[]> pubKeys);
}
