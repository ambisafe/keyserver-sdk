package co.ambisafe.keyserver.asset.impl;

import co.ambisafe.keyserver.asset.CryptoAsset;
import co.ambisafe.keyserver.service.ConnectionManager;
import co.ambisafe.keyserver.supernode.SuperNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public abstract class AbstractCryptoAsset implements CryptoAsset {
    protected ConnectionManager connectionManager;

    protected SuperNode superNode;

    @Autowired
    public abstract void setEnvironment(Environment environment);

    @Autowired
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void setSuperNode(SuperNode superNode) {
        this.superNode = superNode;
    }

    public SuperNode getSuperNode() {
        return superNode;
    }
}
