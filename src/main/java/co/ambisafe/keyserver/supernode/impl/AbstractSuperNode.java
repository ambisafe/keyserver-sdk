package co.ambisafe.keyserver.supernode.impl;

import co.ambisafe.keyserver.supernode.SuperNode;
import org.springframework.stereotype.Component;

import java.util.Date;

public abstract class AbstractSuperNode implements SuperNode {
    private Boolean failed = false;
    private Date failedAt;

    @Override
    final public void markFailed(Boolean failed) {
        this.failed = failed;

        if(failed) {
            failedAt = new Date();
        }
    }

    @Override
    public Boolean isFailed() {
        return failed;
    }

    @Override
    public Date getFailedAt() {
        return failedAt;
    }
}
