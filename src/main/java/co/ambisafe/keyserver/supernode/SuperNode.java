package co.ambisafe.keyserver.supernode;

import co.ambisafe.keyserver.exception.SuperNodeException;

import java.util.Date;
import java.util.List;

public interface SuperNode {
    String sendToNetwork(String rawTxHex) throws SuperNodeException;

    String getTransaction(String id) throws SuperNodeException;

    Long getLatestBlock() throws SuperNodeException;

    List<String> getRawTransactions(long block) throws SuperNodeException;


    Object getBalance(String... args) throws SuperNodeException;

    String getSuperNodeName();

    void markFailed(Boolean failed);

    Boolean isFailed();
    Date getFailedAt();
}