package co.ambisafe.keyserver.exception;

public class SignTransactionException extends RuntimeException {

    public SignTransactionException(String description) {
        super(description);
    }

    public SignTransactionException(String description, Throwable e) {
        super(description, e);
    }
}
