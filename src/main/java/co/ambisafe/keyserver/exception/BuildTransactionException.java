package co.ambisafe.keyserver.exception;

public class BuildTransactionException extends RuntimeException {
    public BuildTransactionException() {
    }

    public BuildTransactionException(String message) {
        super(message);
    }

    public BuildTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public BuildTransactionException(Throwable cause) {
        super(cause);
    }

    public BuildTransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
