package co.ambisafe.keyserver.exception;

public class IgnorableSuperNodeException extends SuperNodeException {
    public IgnorableSuperNodeException() {
    }

    public IgnorableSuperNodeException(String message) {
        super(message);
    }

    public IgnorableSuperNodeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IgnorableSuperNodeException(Throwable cause) {
        super(cause);
    }

    public IgnorableSuperNodeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
