package co.ambisafe.keyserver.exception;

public class BroadcastTransactionException extends SuperNodeException {
    public BroadcastTransactionException() {
    }

    public BroadcastTransactionException(String message) {
        super(message);
    }

    public BroadcastTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public BroadcastTransactionException(Throwable cause) {
        super(cause);
    }

    public BroadcastTransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
