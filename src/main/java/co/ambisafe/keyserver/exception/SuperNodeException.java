package co.ambisafe.keyserver.exception;

public class SuperNodeException extends RuntimeException {
    public SuperNodeException() {
    }

    public SuperNodeException(String message) {
        super(message);
    }

    public SuperNodeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SuperNodeException(Throwable cause) {
        super(cause);
    }

    public SuperNodeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
