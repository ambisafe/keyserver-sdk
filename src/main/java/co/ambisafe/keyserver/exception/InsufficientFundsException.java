package co.ambisafe.keyserver.exception;

public class InsufficientFundsException extends IgnorableSuperNodeException {

    public InsufficientFundsException(String description) {
        super(description);
    }

    public InsufficientFundsException(Throwable cause) {
        super(cause);
    }
}