package co.ambisafe.keyserver.exception;

public class KeyCompromisedException extends RuntimeException {
    public KeyCompromisedException() {
    }

    public KeyCompromisedException(String message) {
        super(message);
    }

    public KeyCompromisedException(String message, Throwable cause) {
        super(message, cause);
    }

    public KeyCompromisedException(Throwable cause) {
        super(cause);
    }

    public KeyCompromisedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
