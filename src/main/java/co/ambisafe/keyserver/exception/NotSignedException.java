package co.ambisafe.keyserver.exception;

public class NotSignedException extends RuntimeException {
    public NotSignedException() {
    }

    public NotSignedException(String message) {
        super(message);
    }

    public NotSignedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotSignedException(Throwable cause) {
        super(cause);
    }

    public NotSignedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
